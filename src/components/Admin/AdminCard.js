import {Button, Row, Col, Table, Modal, Form, ButtonGroup} from 'react-bootstrap'
import { NavLink} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'

export default function AdminCard({product}){
console.log(product)

const {_id} = product

	// For modal
const [show, setShow] = useState(false);
const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const navigate = useNavigate()

// const [NewProdName, setNewProdName] = useState('')
// const [newDescription, setNewDescription] = useState('')
// const [newPrice, setNewPrice] = useState('')
const [inStock, setInStock] = useState(false)
const [rerender, setRerender] = useState(false)

const [prodName, setProdName] = useState('')
const [description, setDescription] = useState('')
const [price, setPrice] = useState(0)
const [isActive, setIsActive] = useState(false)


	function UpdateProduct(event){
	event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/update`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/JSON',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				prodName: prodName,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result !== false){
				setProdName('')
				setDescription('')
				setPrice('')
                setRerender(!rerender)
                handleClose()
				Swal.fire ({
					title: 'Success!',
					icon: 'success',
					text: 'Product successfully updated!'
				})
			// return(
			// 	<Navigate to="/admin"/>
			// )
			} else {
				Swal.fire ({
					title: 'Oh no!',
					icon: 'error',
					text: 'Something went wrong!'
				})
			}
		})
	}
    useEffect(() => {
            fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`)
                .then(response => response.json())
                .then(result => {
                    setProdName(result.prodName)
                    setDescription(result.description)
                    setPrice(result.price)
                    setIsActive(result.isActive)
                })
    }, [rerender])

	const Archive = () =>{
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result !== false){
                setRerender(!rerender)
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Product successfully archived!'
                })

            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

    useEffect(() => {
            fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`)
                .then(response => response.json())
                .then(result => {
                    setProdName(result.prodName)
                    setDescription(result.description)
                    setPrice(result.price)
                    setIsActive(result.isActive)
                })
    }, [rerender])

    const Enable = () =>{
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/enable`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result !== false){
                setRerender(!rerender)
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Product now available!'
                })

            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

    useEffect(() => {
            fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`)
                .then(response => response.json())
                .then(result => {
                    setProdName(result.prodName)
                    setDescription(result.description)
                    setPrice(result.price)
                    setIsActive(result.isActive)
                })
    }, [rerender])

    return(
    	<>
    		<tr>
            <td>{_id}</td>
            <td>{prodName}</td>
            <td>{description}</td>
            <td>₱{price}</td>
            <td>
                {isActive ? "In Stock": "Out of Stock"}
            </td>
	        <td>
	            <Button className="ms-5 mb-1" onClick={handleShow}>Update</Button>
	            {(isActive) ?
                
	            <Button className="ms-5" variant ="danger" onClick={Archive}>Archive</Button>
	            :
	            <Button className="ms-5" variant ="success" onClick={Enable}>Enable</Button>
	            }
	        </td>
		    <Form>
        	<Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter the updated Product Name"
                            value={prodName}
                            onChange={event => setProdName(event.target.value)}
                            required
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter the updated Product Description"
                                value={description}
                                onChange={event => setDescription(event.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter the updated Product Price"
                                value={price}
                                onChange={event => setPrice(event.target.value)}
                                required
                            />
                        </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={event => UpdateProduct(event)}>
                        Update
                    </Button>
                </Modal.Footer>
            </Modal>
        </Form>
        </tr>
        </>
	)            
}

AdminCard.propTypes = {
    product: PropTypes.shape({
        prodName: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

// <Button variant="primary" onClick={handleShow}>Update</Button>{' '}
// <Button variant="danger" as={NavLink} to="/products">Archive</Button>{' '}
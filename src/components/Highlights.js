import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3 text-center">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<img src={"https://images.pexels.com/photos/11935539/pexels-photo-11935539.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"} alt="keyboard image"/>
					<Card.Body>
					    <Card.Title>
					        <h2>Razer Pro Type Ultra</h2>
					            </Card.Title>
					            <Card.Text className="mt-3 text-center">
					              Razer Pro Type Ultra is designed for office use, so it prioritizes comfort and office functionality but keeps the split-millisecond latency that Razer's known for, making it a versatile choice for both work and play. In terms of its office features, it has a comfortable, detachable wrist rest and soft-touch covering on its keycaps to provide extra grip. It's also a wireless unit with a battery life of over 200 hours when you turn the backlighting off.
					            </Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<img src={"https://images.pexels.com/photos/4295360/pexels-photo-4295360.png?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"} alt="an image of speakers"/>
					<Card.Body>
					<Card.Title>
					   <h2>XE300 X-Series Portable Wireless Speaker</h2>
					</Card.Title>
					   <Card.Text className="mt-3">
					       The XE300 speaker is perfect for bringing quality sound to your party. The Line-Shape Diffuser sends a wide sound all around, without compromising on quality. Plus, with a long battery life, portable size and impressive durability, this speaker is the perfect partner for your next party, wherever that might be.  
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<img src={"https://images.pexels.com/photos/1649771/pexels-photo-1649771.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"} alt="an image of a gaming headset"/>
					<Card.Body>
					    <Card.Title>
					    <h2>HyperX Cloud Alpha Gaming Headset</h2>
					    </Card.Title>
					    <Card.Text className="mt-3">
			        	The HyperX Cloud Alpha is perhaps the most praised thing to exist on our products. The biggest drawcard for this headset is the new huge battery. You're looking at up to 300 hours of battery life in wireless mode while still keeping the headset relatively light and comfortable.    
					    </Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
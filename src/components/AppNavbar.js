import {Navbar, Nav, NavDropdown} from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { Link, NavLink } from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar() {
// localStorage - to store user data. Default used in Javascript.
const {user} = useContext(UserContext)
	console.log(user.isAdmin)
	return(
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
		     <Container>
		       <Navbar.Brand as={Link} to="/">ARKHAM Shop</Navbar.Brand>
		       <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		       <Navbar.Collapse id="responsive-navbar-nav">
		         <Nav className="me-auto">
		         { (user.isAdmin) ?
		           <Nav.Link as={NavLink} to="/admin">Admin</Nav.Link>
		          :
		          	<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
		          	
		         }
		         </Nav>
		         <Nav>
		        { (user.id) ? 
		           		
                    (user.isAdmin === false) ?
                    <>
                    <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                    </>
                    :
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  
				:

                  <>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  </>
                }
		         </Nav>
		       	</Navbar.Collapse>

		     </Container>
		   </Navbar>
	)
}

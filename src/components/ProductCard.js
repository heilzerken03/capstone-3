import {useState, useEffect} from 'react'
import {Card, Button, Col, Row, Form, Container} from 'react-bootstrap'
import PropTypes from 'prop-types'
// import UserContext from '../UserContext'
import {Link, useParams} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function ProductCard({product}){
// Destructuring the props
const {prodName, description, price, _id, isActive} = product

  return (

    // <Row className="prods">
    // <Container className="prod">
      // <Col lg={{ span: 4 }} className="prods">
      <div className="prods col-md-3">
        <Card className="prod mt-2 mb-3 p-3 px-2">
          <Card.Title className="prodsTitle text-center md:w-500 p-2 my-2" style={{textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'wrap'}}>
            <h4 className="bg-light shadow-lg hover:shadow-xl rounded-lg">{prodName}</h4>
          </Card.Title>
          <Card.Body>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>₱{price}</Card.Text>
            <Card.Subtitle className="mb-3">{isActive ? "In Stock": "Out of Stock"}</Card.Subtitle>

            <Link className="btn bg-primary text-light" to={`/product/${_id}`} type="button">Details</Link>

          </Card.Body>
        </Card>
      </div>
  ) 
}   

ProductCard.propTypes = {
	 product: PropTypes.shape({
		prodName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
    // isActive: PropTypes.boolean.isRequired
	})
}
// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign and a specific validation for each of them.

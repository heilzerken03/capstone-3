import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button, Row, Col, InputGroup, ButtonGroup} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserOrders from '../pages/UserOrders'
import OrderCard from '../components/OrderCard'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView() {
	// Gets the courseId from the URL of the route that this component is connected to '/courses/:courseId'
	const navigate = useNavigate()

	const {productId} = useParams()
	const {user} = useContext(UserContext)

	const [prodName, setProdName] = useState('')
	const [price, setPrice] = useState(0)
	// const [quantity, setQuantity] = useState(0)
	const [totalAmount, setTotalAmount] = useState(0)
	const [isActive, setIsActive] = useState(false)

	// Product View
	const [description, setDescription] = useState('')

	// Create Order
	// const {userId, prodName, products, totalAmount, purchasedOn} = order

	const [count, useCount] = useState(0)

		const Increment = () => {
			useCount(count + 1)
		}
		const Decrement = () => {
			let value = 0
			if(count <= value)
			{
				value = 1
			} else {
				value = count
			}
			useCount(value - 1)
		}

	const createOrder = () => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/order-product`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				prodName: prodName,
				productId: productId,
				quantity: count


			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Order has been created!"
				})

				navigate('/orders')
			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setProdName(result.prodName)
			setDescription(result.description)
			// setQuantity(result.quantity)
			setPrice(result.price)
			setIsActive(result.isActive)
		})
	}, [productId])
		return(
		// FOR VIEWING SINGLE PRODUCT
			<Container className="mt-5">
				<Row>
					<Col lg={{ span: 6, offset: 3}}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title className="mb-3 bg-light"><h4>{prodName}</h4></Card.Title>
								<Card.Subtitle >Description:</Card.Subtitle>
								<Card.Text className="mb-3">{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text className="mb-3">₱{price}</Card.Text>
								<Card.Subtitle className="mb-3">Quantity:</Card.Subtitle>
								<ButtonGroup className="mb-3">
								<Button onClick={() => Decrement()}>-</Button>
								<InputGroup type="text" className="form-control">{count}</InputGroup>
								<Button onClick={() => Increment()}>+</Button>
								</ButtonGroup>
								<Card.Subtitle className="mb-3">{isActive ? "In Stock" : "Out of Stock"}</Card.Subtitle>
								{	user.id !== null ?
										<Button variant="primary" onClick={() => createOrder(productId)}>Purchase</Button>
									:
										<Link className = "btn btn-secondary btn-block" to="/login">Login to Order</Link>
								}
							</Card.Body>		
						</Card>
					</Col>
				</Row>
		</Container>
	)
}
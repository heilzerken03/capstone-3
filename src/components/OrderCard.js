import {Button, ButtonGroup, InputGroup, Row, Col, Table, Form} from 'react-bootstrap'
import { NavLink} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Loading from '../components/Loading'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'

export default function OrderCard({order}) {

const {user} = useContext(UserContext)
const [name, setName] = useState('')

	useEffect(() => {
    	let fetchUrl = `${process.env.REACT_APP_API_URL}/users/${user.id}/details`

    	fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/details`, {
    		headers: {
    			"Content-Type": "application/json",
    			Authorization: `Bearer ${localStorage.getItem('token')}`
    		}
    	})
    	.then(response => response.json())
    	.then(result => {
    		setName(`${result.firstName} ${result.lastName}`)
    		console.log(result)

    	})
	}, [])

	return (
			<>
				<tr className="text-center">
				<td>{order._id}</td>
		        <td>{order.prodName}</td>
		        <td>{
		        	order.products.map(data => {
		        		return(`${data.quantity}`)
		        	})
		        }</td>
		        <td>{
		        	order.products.map(data => {
		        		return(`${data.productId}`)
		        	})
		        }</td>
				<td>₱{order.totalAmount}</td>
				<td>{order.purchasedOn}</td>
				<td>
		        	{order.userId}
		        </td>
		        </tr>
		    </>
    )
}
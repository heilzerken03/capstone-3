import {Button, Row, Col} from 'react-bootstrap'
import { NavLink} from 'react-router-dom'

export default function Banner(){
	return (
			<Row>
				<Col className="p-5 text-center" >
					<h1>ARKHAM Shop</h1>
					<p>We know want you want.</p>
					<Button variant="primary" as={NavLink} to="/products"><h6>Take a look!</h6></Button>
				</Col>
			</Row>
		
		)
}
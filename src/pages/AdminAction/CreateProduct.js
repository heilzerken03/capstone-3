import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import {Row, Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function CreateProduct(){
// const {user} = useContext(UserContext)
const navigate = useNavigate()

const [prodName, setProdName] = useState('')
const [description, setDescription] = useState('')
const [price, setPrice] = useState(0)
const [isActive, setIsActive] = useState(false)

function AddProduct(event){
event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/products/create-product`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/JSON',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			prodName: prodName,
			description: description,
			price: price
		})
	})
	.then(response => response.json())
	.then(result => {
		if(result !== false){
			setProdName('')
			setDescription('')
			setPrice('')

			Swal.fire ({
				title: 'Success!',
				icon: 'success',
				text: 'New product successfully created!!'
			})

			navigate('/admin')
		} else {
			Swal.fire ({
				title: 'Oh no!',
				icon: 'error',
				text: 'Something went wrong!'
			})
		}
	})
}

useEffect(() => {
	if((prodName !== '' && description !== '' && price !== '')){
		// Enables the submit button if the form data has been verified
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [prodName, description, price])
	
	return(
		<Container className="mt-3 mb-3 p-3 my-5 flex-column justify-content-center align-items-center w-50">
		<Row className="mt-3 mb-3">
		<Form onSubmit = {event => AddProduct(event)} className="createProd">
		<Form.Label><h3 className="text-center">Create New Product</h3></Form.Label>
			  <Form.Group controlId="prodName">
		        <Form.Label>Product Name</Form.Label>
		        <Form.Control 
			        type ="text" 
			        placeholder ="Enter the product name" 
			        value = {prodName}
			        onChange = {event => setProdName(event.target.value)}
			        required
		        />
		      </Form.Group>
		      <Form.Group controlId="description">
		        <Form.Label className="mt-3 mb-2">Product Description</Form.Label>
		        <Form.Control 
			        type ="text" 
			        placeholder ="Enter the product description" 
			        value = {description}
			        onChange = {event => setDescription(event.target.value)}
			        required
		        />
		      </Form.Group>
		        <Form.Group controlId="price">
		        <Form.Label className="mt-3 mb-2">Price</Form.Label>
		        <Form.Control 
	                type ="text" 
	                placeholder ="Price"
	                value = {price}
			        onChange = {event => setPrice(event.target.value)}
	                required
		        />
		        </Form.Group>
				<Button className ="mt-4" variant="primary" type="submit" id="submitBtn">
		            Confirm New Product
				</Button>
		</Form>
		</Row>
		</Container>
	)
}
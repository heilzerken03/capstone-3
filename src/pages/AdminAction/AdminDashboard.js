import {Button, Row, Col, Table} from 'react-bootstrap'
import { NavLink} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import Loading from '../../components/Loading'
import AdminCard from '../../components/Admin/AdminCard'
import PropTypes from 'prop-types'
import UserContext from '../../UserContext'
import CreateProduct from '../AdminAction/CreateProduct'

export default function AdminDashboard(){
	const {user} = useContext(UserContext)
	const navigate = useNavigate()

  	const [products, setProducts] = useState([])
    const [isLoading, setIsLoading] = useState(false)
	const [refetchData, setRefetchData] = useState(false)


	useEffect(() => {
        setIsLoading(true)

        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(response => response.json())
        .then(result => {
            setProducts(
                result.map(product => {
                    return(
                        <AdminCard key={product._id} product={product}/>
                    )
                })
            )

        	setIsLoading(false)

        })
	}, [])

	// function Refresh(){
	//     useEffect(() => {
	//          setRefetch(/products)
	//     }, [refetchData])
	// }

	return (
		(isLoading) ?
            <Loading/>
        :
        user.isAdmin === true ?

			<Row className="bg-light ml-3">
				<Col className="p-5 text-center">
					<h1>ADMIN Dashboard</h1>
					<Button variant="primary" as={NavLink} to="/admin/create-product">Create Product</Button>{' '}
					<Button variant="warning" as={NavLink} to="/products">Products</Button>
				</Col>
				<Table striped bordered hover variant="dark">
				      <thead className="">
				        <tr className="text-center">
				          <th>Product ID</th>
				          <th>Product Name</th>
				          <th>Description</th>
				          <th>Price</th>
				          <th>Availability</th>
				          <th>Actions</th>
				        </tr>
				      </thead>
				      <tbody>
						{products}
				      </tbody>
				</Table>
			</Row>
			:
            navigate('/')
	)
}	

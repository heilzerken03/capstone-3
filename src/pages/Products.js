import ProductCard from '../components/ProductCard'
import {useEffect, useState} from 'react'
import Loading from '../components/Loading'
import ProductBanner from '../components/ProductBanner'
// import products_data from '../data/products'

export default function Products(){

	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		// Sets the loading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(result => {

			setProducts(
				result.map(product => {
					return (
						<ProductCard key={product.id} product={product}/>
					)
				})
			)

			setIsLoading(false)
		})
	}, [])
	// .map for looping and iteration of array.
	return(
		(isLoading) ?
			<Loading/>
		:
			<>
				<div className="user_products">
				<ProductBanner/>
				{products}
				</div>
			</>
	)	
}
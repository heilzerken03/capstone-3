import {useState, useEffect, useContext} from 'react'
import {Row, Form, Button, Card, Container, Col} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
// Initializes the use of the properties from the UserProvided in App.js file
const {user, setUser} = useContext(UserContext)

const [email, setEmail] = useState('')
const [password, setPassword] = useState('')
// Initialize useNavigate
const navigate = useNavigate()

const [isActive, setIsActive] = useState(false)
// After login from AppNavBar, it will be directed to Home
// Get authorized user's details
const retrieveUser = (token) => {
	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		headers: {
			// Authorization: `Bearer ${token}`
			Authorization: `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(response => response.json())
	.then(result => {
		console.log(result)

		// Store the user details retrieved from the token into the global user state
		setUser({
			id: result._id,
			isAdmin: result.isAdmin
		})
	})
}
// For Access Token
function authenticate(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			email: email,
			password: password,
		})
	})
	.then(response => response.json())
	.then(result => {
		if(typeof result.accessToken !== "undefined"){
			localStorage.setItem('token', result.accessToken)

			retrieveUser(result.accessToken)

			Swal.fire({
				title: 'Login Successful!',
				icon: 'success',
				text: "Welcome to ARKHAM Shop!"
			})
		} else {
			Swal.fire({
				title: 'Authentication Failed!',
				icon: 'error',
				text: 'Sorry, please try again.'
			})
		}
	})	
}

useEffect(() => {
	if(email !=='' && password !==''){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password])

console.log()

	return(
		(user.id !== null) ?
				<Navigate to ="/"/>
		:
			<Container className="loginCard my-5 d-flex flex-column justify-content-center align-items-center">
			<Form className="rounded col-md-8 col-lg-4 col-11" onSubmit ={event => authenticate(event)}>
				<Card.Header className="mb-3 text-center"><h3>Login</h3></Card.Header>
			     <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
				        type ="email" 
				        placeholder ="Enter email" 
				        value = {email}
				        onChange = {event => setEmail(event.target.value)}
				        required
			        />
			        </Form.Group>

			        <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
		                type ="password" 
		                placeholder ="Password"
		                value = {password}
				        onChange = {event => setPassword(event.target.value)}
		                required
			        />
			        </Form.Group>

				{ isActive ?
					<Button className ="mt-2 mb-3" variant="primary" type="submit" id="submitBtn">
			            Submit
					</Button>
					:
					<Button className ="mt-2 mb-3" variant="primary" type="submit" id="submitBtn" disabled>
			            Submit
					</Button>
				}
			</Form>
			</Container>
		)
}
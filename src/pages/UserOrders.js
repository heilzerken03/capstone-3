import {useNavigate, Navigate} from 'react-router-dom'
import {useEffect, useState, useContext} from 'react'
import {Button, Row, Col, Table, Form} from 'react-bootstrap'
import Loading from '../components/Loading'
import OrderCard from '../components/OrderCard'
import OrderBanner from '../components/OrderBanner'
import UserContext from '../UserContext'

export default function UserOrders(){

	const navigate = useNavigate()
	const {user} = useContext(UserContext)

  	const [orders, setOrders] = useState([])
    const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
        setIsLoading(true)
    	let fetchUrl = `${process.env.REACT_APP_API_URL}/orders/${user.id}/user-orders`
    	if(user.isAdmin === true) {

    		fetchUrl = `${process.env.REACT_APP_API_URL}/orders/all-orders`
    	}

        fetch(fetchUrl, {
        	headers: {
        		"Content-Type": "application/json",
        		Authorization: `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(response => response.json())
        .then(result => {
        	console.log(result)
            setOrders(
                result.map(order => {
                    return(
                        <OrderCard key={order._id} order={order}/>
                    )
                })
            )

        	setIsLoading(false)

        })
	}, [])

	return (
		(isLoading) ?
	        <Loading/>
	    :
	    user.id !== null ?

			<Row className="bg-light ml-3">
				<OrderBanner/>
				<Table striped bordered hover variant="light">
				      <thead>
				        <tr className="text-center">
				          <th>Order ID</th>	
				          <th>Product Name</th>  
				          <th>Quantity</th>
				          <th>Product ID</th>
				          <th>Subtotal</th>
				          <th>Ordered On</th>
				          <th>User ID</th>
				        </tr>
				      </thead>
				      <tbody>
						{orders}
				      </tbody>
				</Table>
			</Row>
			:
            navigate('/')
	)
}
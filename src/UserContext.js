import React from 'react'

// Initializes a react context
const UserContext = React.createContext()
// Initializes a context provider
// gives us ability to provide a specific context through a component

// to include the variable UserProvider when the UserContext object is exported
export const UserProvider = UserContext.Provider
// to export the whole file
export default UserContext
